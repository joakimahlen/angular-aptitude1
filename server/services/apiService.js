var Q = require('q');

module.exports = {
    getTodos: function() {
        var deferred = Q.defer();

        deferred.resolve([
            {
                id: 1,
                name: "The first task",
                deadline: "2017-02-14T14:35:01Z"
            },
            {
                id: 2,
                name: "The second task",
                deadline: "2017-01-01T14:35:01Z"
            },
            {
                id: 3,
                name: "The third task",
                deadline: "2017-02-28T14:35:01Z"
            },
            {
                id: 4,
                name: "The fourth task",
                deadline: "2018-01-01T14:35:01Z"
            },
            {
                id: 5,
                name: "The fifth task",
                deadline: "2017-08-01T14:35:01Z"
            },
        ]);

        return deferred.promise;
    },
};
