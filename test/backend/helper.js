/*jshint browser: false, strict: false, expr: true*/
/*global console, require, module, Report */

global.expect = require('chai').expect;
var mongoose = require('mongoose');

var mockgoose = require('mockgoose');
mockgoose(mongoose).then(function() {
    // mongoose connection
});

global.setUpTestMongoDb = function(done) {
    if (mongoose.connection.readyState === 0) {
        console.log("-- Connection to test database and dropping data!");
        mongoose.connect("mongodb://localhost/alotonline-test");
        mongoose.connection.on('connected', function () {
            mongoose.connection.db.dropDatabase(function (err, result) {
                done();
            });
        });
    } else {
        done();
    }
};

global.wrapExceptionFn = function(fn, arg) {
    // Execute a function. If no exception occurrs, return the data the function returns.
    // If an exception occurrs, return the error message instead of the function return.
    // If no function exists, return undefined;
    if (fn) {
        try {
            return fn(arg);
        } catch (e) {
            return e;
        }
    } else {
        return;
    }
};

global.testPromise = function(promise, done, checkFn) {
    promise.then(function(result) {
        var fnResult = wrapExceptionFn(checkFn, result);
        done(fnResult);
    }).catch(function(promiseRejectionError) {
        var fnResult = wrapExceptionFn(checkFn, promiseRejectionError);
        done(fnResult);
    });
};

global.testMongodbPromiseDuplicate = function(promise, done) {
    testPromise(promise, done, function(error) {
        expect(error.code).to.equal(11000);
    });
};

global.testMongodbPromiseMissingRequiredField = function(promise, fields, done) {
    testPromise(promise, done, function(error) {
        expect(error.name).to.equal('ValidationError');
        var errorsSorted = Object.keys(error.errors).sort();
        var fieldsSorted = fields.sort();

        expect(errorsSorted).to.deep.equal(fieldsSorted);
    });
};
