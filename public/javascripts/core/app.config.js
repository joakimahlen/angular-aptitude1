(function () {
    'use strict';

    angular.module('app').config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider', '$qProvider'];

    function config($stateProvider, $urlRouterProvider, $locationProvider, $qProvider) {

        $qProvider.errorOnUnhandledRejections(false);
        //$urlRouterProvider.otherwise('/error');
        $urlRouterProvider.otherwise(function($injector, $location){
            var $state = $injector.get('$state');

            $state.go('error', { message: "Error 404! Page '" + $location.path() + "' not found." });
        });

        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'partials/home.html',
                controller: 'homeController as vm'
            })
            .state('error', {
                url: '/error',
                templateUrl: 'partials/error.html',
                controller: 'errorController as vm',
                params: { message: "Page not found!" }
            });

        $locationProvider.html5Mode(true);
    }

})();