var express = require('express');
var api = express.Router();
var apiService = require('../server/services/apiService');

api.get('/todos', function(req, res) {
    apiService.getTodos().then(function(todos) {
        res.send(todos);
    }).catch(function(error) {
        res.statusCode = 500;
        res.send({
            message: "Error getting todos!",
            error: error
        });
    });

});

module.exports = api;
